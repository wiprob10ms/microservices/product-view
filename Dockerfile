# Alpine because it's lighter
FROM openjdk:8-jdk-alpine
MAINTAINER Victor Blosquievis <victor.blosquievis@wipro.com>

# Set ENV variables
ENV PORT=8080

# Add JAR file and run it as entrypoint
ADD target/product-view-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

# Expose the port
EXPOSE 8080