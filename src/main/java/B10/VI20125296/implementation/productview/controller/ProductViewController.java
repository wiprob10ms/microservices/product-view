package B10.VI20125296.implementation.productview.controller;

import B10.VI20125296.implementation.productview.dto.ProductDTO;
import B10.VI20125296.implementation.productview.service.ProductViewService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product-view")
public class ProductViewController {

    private ProductViewService productViewService;

    public ProductViewController(ProductViewService productViewService){
        this.productViewService = productViewService;
    }

    @GetMapping("/{productCode}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable String productCode){
        return this.productViewService.getProductDetails(productCode);
    }
}
