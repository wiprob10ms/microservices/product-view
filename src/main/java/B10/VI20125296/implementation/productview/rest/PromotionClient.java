package B10.VI20125296.implementation.productview.rest;

import B10.VI20125296.implementation.productview.dto.PromotionDTO;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface PromotionClient {
    @RequestLine("GET /products/{productCode}")
    List<PromotionDTO> getPromotion(@Param("productCode") String productCode);
}
