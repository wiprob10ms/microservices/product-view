package B10.VI20125296.implementation.productview.rest;

import B10.VI20125296.implementation.productview.dto.ProductDTO;
import feign.Param;
import feign.RequestLine;

public interface ProductClient {
    @RequestLine("GET /products/{productCode}")
    ProductDTO getProduct(@Param("productCode") String productCode);
}
