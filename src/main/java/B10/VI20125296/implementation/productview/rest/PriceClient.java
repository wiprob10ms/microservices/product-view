package B10.VI20125296.implementation.productview.rest;

import B10.VI20125296.implementation.productview.dto.PriceDTO;
import feign.Param;
import feign.RequestLine;

public interface PriceClient {
    @RequestLine("GET /products/{productCode}")
    PriceDTO getPrice(@Param("productCode") String productCode);
}
