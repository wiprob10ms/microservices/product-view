package B10.VI20125296.implementation.productview.rest;

import B10.VI20125296.implementation.productview.dto.InventoryDTO;
import feign.Param;
import feign.RequestLine;

public interface InventoryClient {
    @RequestLine("GET /inventory/{productCode}")
    InventoryDTO getInventoryByProductCode(@Param("productCode") String productCode);
}
