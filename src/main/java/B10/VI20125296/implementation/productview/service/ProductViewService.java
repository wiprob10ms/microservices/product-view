package B10.VI20125296.implementation.productview.service;

import B10.VI20125296.implementation.productview.dto.InventoryDTO;
import B10.VI20125296.implementation.productview.dto.PriceDTO;
import B10.VI20125296.implementation.productview.dto.ProductDTO;
import B10.VI20125296.implementation.productview.dto.PromotionDTO;
import B10.VI20125296.implementation.productview.rest.InventoryClient;
import B10.VI20125296.implementation.productview.rest.PriceClient;
import B10.VI20125296.implementation.productview.rest.ProductClient;
import B10.VI20125296.implementation.productview.rest.PromotionClient;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import feign.Feign;
import feign.FeignException;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class ProductViewService {

    private static final String PRODUCT_END_POINT = "/product-service";
    private static final String PRODUCT_CACHE_KEY = "/PRODUCT/";

    private static final String PROMOTION_END_POINT = "/meru-promotions";
    private static final String PROMOTION_CACHE_KEY = "/PROMOTION/";

    private static final String INVENTORY_END_POINT = "/meru-inventory";
    private static final String INVENTORY_CACHE_KEY = "/INVENTORY/";

    private static final String PRICE_END_POINT = "/meru-price";
    private static final String PRICE_CACHE_KEY = "/PRICE/";


    @Value("${meru.zuul.uri}")
    private String zuulUri;

    private Cache<String, Object> cache;

    public ProductViewService(){
        this.cache = Caffeine.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES).build();
        this.cache.cleanUp();
    }

    public ResponseEntity<ProductDTO> getProductDetails(String productCode){

        ProductDTO productDTO = this.getProduct(productCode);
        if(productDTO == null){
            return ResponseEntity.notFound().build();
        }

        productDTO.setInventory(this.getInventory(productCode));
        productDTO.setPrice(this.getPrice(productCode));
        productDTO.setPromotion(this.getPromotions(productCode));

        return ResponseEntity.ok(productDTO);
    }

    private ProductDTO getProduct(String productCode){
        ProductDTO productDTO;

        try {
            ProductClient productClient = Feign.builder()
                    .encoder(new JacksonEncoder())
                    .decoder(new JacksonDecoder())
                    .target(ProductClient.class, this.zuulUri + PRODUCT_END_POINT);

            productDTO = productClient.getProduct(productCode);
            this.cache.put(PRODUCT_CACHE_KEY.concat(productCode), productDTO);
        }catch (FeignException e){
            log.error("Impossible to reach PRODUCT, getting information from cache");
            productDTO = (ProductDTO) this.cache.getIfPresent(PRODUCT_CACHE_KEY.concat(productCode));

            if(productDTO == null){
                log.error("Not found on cache, verify product code");
            }
        }

        return productDTO;
    }

    private List<PromotionDTO> getPromotions(String productCode){
        List<PromotionDTO> promotionDTO;

        try {
            PromotionClient promotionClient = Feign.builder()
                    .encoder(new JacksonEncoder())
                    .decoder(new JacksonDecoder())
                    .target(PromotionClient.class, this.zuulUri + PROMOTION_END_POINT);

            promotionDTO = promotionClient.getPromotion(productCode);
            this.cache.put(PROMOTION_CACHE_KEY.concat(productCode), promotionDTO);
        }catch (FeignException e){
            log.error("Impossible to reach PROMOTION, getting information from cache");
            promotionDTO = (List<PromotionDTO>) this.cache.getIfPresent(PROMOTION_CACHE_KEY.concat(productCode));

            if(promotionDTO == null){
                log.error("Not found on cache, setting promotions to null");
            }
        }

        return promotionDTO;
    }

    private PriceDTO getPrice(String productCode){
        PriceDTO priceDTO;

        try {
            PriceClient priceClient = Feign.builder()
                    .encoder(new JacksonEncoder())
                    .decoder(new JacksonDecoder())
                    .target(PriceClient.class, this.zuulUri + PRICE_END_POINT);

            priceDTO = priceClient.getPrice(productCode);
            this.cache.put(PRICE_CACHE_KEY.concat(productCode), priceDTO);
        }catch (FeignException e){
            log.error("Impossible to reach PRICE, getting information from cache");
            priceDTO = (PriceDTO) this.cache.getIfPresent(PRICE_CACHE_KEY.concat(productCode));

            if(priceDTO == null){
                log.error("Not found on cache, setting price to 0 and quantity to 0");
            }
        }

        return priceDTO;
    }

    private InventoryDTO getInventory(String productCode){
        InventoryDTO inventoryDTO;

        try {
            InventoryClient inventoryClient = Feign.builder()
                    .encoder(new JacksonEncoder())
                    .decoder(new JacksonDecoder())
                    .target(InventoryClient.class, this.zuulUri + INVENTORY_END_POINT);

            inventoryDTO = inventoryClient.getInventoryByProductCode(productCode);
            this.cache.put(INVENTORY_CACHE_KEY.concat(productCode), inventoryDTO);
        }catch (FeignException e){
            log.error("Impossible to reach INVENTORY, getting information from cache");
            inventoryDTO = (InventoryDTO) this.cache.getIfPresent(INVENTORY_CACHE_KEY.concat(productCode));

            if(inventoryDTO == null){
                log.error("Not found on cache, setting quantity to 0");
            }
        }

        return inventoryDTO;
    }
}
