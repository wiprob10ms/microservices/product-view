package B10.VI20125296.implementation.productview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
public class ProductViewApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductViewApplication.class, args);
    }
}
