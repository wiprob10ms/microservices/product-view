package B10.VI20125296.implementation.productview.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PriceDTO {
    private Long id;
    private String productId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private BigDecimal price;
}
