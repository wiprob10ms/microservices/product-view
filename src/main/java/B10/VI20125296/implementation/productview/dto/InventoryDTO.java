package B10.VI20125296.implementation.productview.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InventoryDTO {
    private String productCode;
    private BigDecimal stockQuantity;
}
