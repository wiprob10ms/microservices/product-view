package B10.VI20125296.implementation.productview.dto;

public enum DiscountType {
    VALUE, PERCENTAGE
}
