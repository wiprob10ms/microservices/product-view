package B10.VI20125296.implementation.productview.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductDTO {

    private String productCode;
    private String name;
    private String brand;
    private String description;
    private String category;
    private int measureValue;
    private String measureUnit;
    private BigDecimal basePrice;
    private String currency;
    private BigDecimal stockQuantity;
    private List<PromotionDTO> promotion;

    public void setInventory(InventoryDTO inventoryDTO){
        if(inventoryDTO != null){
            this.stockQuantity = inventoryDTO.getStockQuantity();
        }else{
            this.stockQuantity = new BigDecimal(0);
        }
    }

    public void setPrice(PriceDTO price){
        if(price != null){
            this.basePrice = price.getPrice();
        }else{
            this.basePrice = new BigDecimal(0);
            this.stockQuantity = new BigDecimal(0);
        }
    }

}
