package B10.VI20125296.implementation.productview.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

@Data
public class PromotionDTO {
    private Long id;
    private String name;
    private String description;
    private Set<String> products;
    private BigDecimal discountAmount;
    private DiscountType discountType;
    private Date startDateTime;
    private Date endDateTime;
}
